<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\request;

class Index extends Controller
{
	protected function _initialize() {
		$this->assign('action',$this->request->action());
	}
	
    public function index()
    {

		$this->assign('xuexiao',db('gw_news')->where('pid',4)->limit(3)->select());//学院动态
		$this->assign('shiz',db('gw_news')->where('pid',5)->limit(6)->select());//行业前景
		$this->assign('rongyao',db('gw_alist')->where('pid',3)->limit(11)->select());//
		
		$this->assign('lil',db('gw_alist')->where('pid',2)->limit(6)->select());//师资力量
    	return view();
	}
    //关于我们
    public function about($id="5",$pid='')
    {
    	
    	$res = db('gw_about')->where('pid',1)->select();
    	$this->assign('list',$res);
    	
    	$about = db('gw_about')->where('id',$id)->find();
    	$this->assign('about',$about);
    	
    	$this->assign('id',$id);
    	
    	$this->assign('pid',$pid);
    	return view();
    }
    //学校简介
    public function alist($pid=2)
    {
    	$about['id']='';
    	$res = Db::name('gw_alist')->where('pid',$pid)->paginate(6, false, get_query());
		$this->assign('data',$res);
		$this->assign('pid',$pid);
		
		$ad = db('gw_ads')->where('action','about')->order('sort asc')->find();
    	$this->assign('ad',$ad);
    	
    	$this->assign('about',$about);
    	return $this->fetch();
    }
    public function ablist($pid=1)
    {
    	$title='学校环境';
		if($pid==3){
			$title='资深荣誉';
		}else if($pid==4){
			$title='企业合作';
		}
    	$about['id']='';
		$this->assign('title',$title);
    	$res = Db::name('gw_alist')->where('pid',$pid)->paginate(5, false, get_query());
		$this->assign('data',$res);
		$this->assign('pid',$pid);
		
		$ad = db('gw_ads')->where('action','about')->order('sort asc')->find();
    	$this->assign('ad',$ad);
    	
    	$this->assign('about',$about);
    	return $this->fetch();
    }
    public function alistshow($id="id")
    {
    	$about['id']="id";
    	$data = db("gw_alist")->where('id',$id)->find();
    	$this->assign('data',$data);
    	$ad = db('gw_ads')->where('action','about')->order('sort asc')->find();
    	$this->assign('ad',$ad);
    	$this->assign('about',$about);
    	return $this->fetch();
    }
    
    //
    public function hotcourse($id="6")
    {
    	
    	$res = db('gw_about')->where('pid',2)->select();
    	$this->assign('list',$res);
    	
    	$about = db('gw_about')->where('id',$id)->find();
    	$this->assign('about',$about);
    	
    	$this->assign('id',$id);
    	return view();
    }
    
     //学院动态
    public function newlist($pid=4,$fid=1)
    {
    	$res = Db::name('gw_news')->where('pid',$pid)->paginate(6, false, get_query());
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',$fid)->select();
		$this->assign('type',$type);
		$this->assign('pid',$pid);
		
    	return $this->fetch();
    }
    public function newshow($id,$pid=4)
    {
    	$type = db('gw_newscate')->where('pid',1)->select();
    	$this->assign('type',$type);
    	
    	$data = db('gw_news')->where('id',$id)->find();
    	db('gw_news')->where('id',$id)->setInc('click',2);
    	$this->assign('data',$data);
    	$this->assign('pid',$pid);
    	return $this->fetch();
    }
    
     //学员就业
    public function joblist($pid=6)
    {
    	$res = Db::name('gw_news')->where('pid',$pid)->paginate(5, false, get_query());
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',2)->select();
		$this->assign('type',$type);
		$this->assign('pid',$pid);
    	return $this->fetch();
    }
    public function jobshow($id,$pid=2)
    {
    	$type = db('gw_newscate')->where('pid',$pid)->select();
    	$this->assign('type',$type);
    	
    	$data = db('gw_news')->where('id',$id)->find();
    	db('gw_news')->where('id',$id)->setInc('click');
    	$this->assign('data',$data);
    	$this->assign('pid',$pid);
    	return $this->fetch();
    }
    //招生问答
    public function answerslist($pid=8,$fid=3,$keyword='')
    {
		$map = array();
		if ($keyword) {
			$map['title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
	
		
		$res = Db::name('gw_alist')->where($map)->order('add_time desc')->paginate(5, false, get_query());
		$this->assign('data',$res);
		
    
		$type = Db::name('gw_newscate')->where('pid',$fid)->select();
		$this->assign('type',$type);
		$this->assign('pid',$pid);
    	return $this->fetch();
    }
    public function answersshow($id,$fid=3)
    {
    	$type = db('gw_newscate')->where('pid',$fid)->select();
    	$this->assign('type',$type);
    	
    	$data = db('gw_news')->where('id',$id)->find();
    	db('gw_news')->where('id',$id)->setInc('click');
    	$this->assign('data',$data);
    	return $this->fetch();
    }
    
     //通知公告
    public function notice()
    {
    	$res = Db::name('gw_message')->where('pid',2)->paginate(5, false, get_query());
		$this->assign('data',$res);
    	return $this->fetch();
    }
    public function noticeshow($id)
    {
    	$data = db('gw_message')->where('id',$id)->find();
    	$this->assign('data',$data);
    	return $this->fetch();
	}
	
    //在线留言
    public function message()
    {
    	
    	if($this->request->isPost()){
			$data = $this->request->post();
			$data['add_time'] = now_time();
			$res = db('gw_message')->insert($data);
			if($res){
				return $this->success('留言成功');
			}else{
				return $this->error('留言失败');
			}
		}else{
	    	return $this->fetch();
		}
    }
    
    //我要报名
    public function baoming()
    {
    	
    	if($this->request->isPost()){
			$data = $this->request->post();
			$data['add_time'] = now_time();
			$data['pid'] = 3;
			$res = db('gw_message')->insert($data);
			if($res){
				return '你已报名成功 稍后我们学校会联系你';
			}else{
				return '报名失败';
			}
		}else{
	    	return $this->fetch();
		}
    }
    
    
}
