<?php
namespace app\mobile\controller;
use think\Controller;
use think\Db;
use think\request;

class Index extends Controller
{
	protected function _initialize() {
		$this->assign('action',$this->request->action());
	}
	
    public function index()
    {	
		if($this->request->isPost()){
			$data = $this->request->post();
			$c['add_time'] = now_time();
			$c['pid'] = 3;
			$c['title']=$data['username'];
			$c['content']=$data['sex'];
			$c['tel']=$data['phone'];
			$c['km']=$data['yc'];
			$cr=Db::name('gw_message')->insert($c);
			if($cr){
				return json(['code'=>3,'message'=>'你已报名成功 稍后我们学校会联系你']);
			}
				return json(['code'=>3,'message'=>'报名失败']);
		}
		//校园动态
		$xiaoy = db('gw_news')->order('id DESC')->limit(5)->select();
		$this->assign('xiaoy',$xiaoy);

		$sj = Db::name('gw_alist')->where('pid',1)->paginate(6, false, get_query());
		$this->assign('sj',$sj);
		$res = Db::name('gw_alist')->where('pid',2)->paginate(6, false, get_query());
		$this->assign('data',$res);
    	return view('index/shouye');
    }
	//提交
	public function shouye(){
		if($this->request->isPost()){
			$data = input('post.');
			$date=[
				'username'=>$data['username'],
				'password'=>md5($data['password'].config('auth_key'))
			];
			$cha=Db::name('auth_admin')->insert($date);
			if($cha){
				return json(['code'=>1,'url'=>'/mobile/index','message'=>'提交成功','aa'=>'21']);
			}
				return json(['code'=>3,'message'=>'提交失败']);
		}
	}
	//教师介绍
	public function view($id='1'){
    	$res = db('gw_alist')->where('id',$id)->find();
    	$this->assign('list',$res);
		return view();
	}
	 //通知公告
    public function notice()
    {
    	$res = Db::name('gw_message')->where('pid',2)->paginate(5, false, get_query());
		$this->assign('data',$res);
    	return $this->fetch();
    }
    //关于我们
    public function about($id='')
    {
    	$res = db('gw_about')->where('pid',1)->select();
    	$this->assign('list',$res);
    	
    	$about = db('gw_about')->where('pid',1)->select();
    	$this->assign('about',$about);
   
    	return view();
    }
    //学校简介
    public function alist($pid,$id='')
    {
    	$about['id']='';
		$zhi='';
			$res = Db::name('gw_alist')->where('pid',$pid)->paginate(4, false, get_query());
		if($id){
			$zhi='123';
    		$res = Db::name('gw_alist')->where('id',$id)->limit(1)->select();
		}
		$this->assign('zhi',$zhi);
		$this->assign('data',$res);
		$this->assign('pid',$pid);
		
		$ad = db('gw_ads')->where('action','about')->order('sort asc')->find();
    	$this->assign('ad',$ad);
    	
    	$this->assign('about',$about);
    	return $this->fetch();
    }
    public function ablist($pid,$id='')
    {
    	$about['id']='';
    	$res = Db::name('gw_alist')->where('pid',$pid)->paginate(5, false, get_query());
			$title='学校环境';
		if($pid==3){
			$title='资深荣誉';
		}else if($pid==4){
			$title='校企合作';
		}

		$zhi='';
		if($id){
			$zhi='123';
    		$res = Db::name('gw_alist')->where('id',$id)->limit(1)->select();
		}
		$this->assign('zhi',$zhi);
		$this->assign('title',$title);
		$this->assign('title',$title);
		$this->assign('data',$res);
		$this->assign('pid',$pid);
		
		$ad = db('gw_ads')->where('action','about')->order('sort asc')->find();
    	$this->assign('ad',$ad);
    	
    	$this->assign('about',$about);
    	return $this->fetch();
    }
	  public function ablistshow($id='')
    {
    	$about['id']='';
    	$data = db('gw_alist')->where('id',$id)->find();
    	$this->assign('data',$data);
    	$ad = db('gw_ads')->where('action','about')->order('sort asc')->find();
    	$this->assign('ad',$ad);
    	$this->assign('about',$about);
    	return $this->fetch();
    }
    
    //
    public function hotcourse($id="6")
    { 
    	
    	$res = db('gw_about')->where('pid',2)->select();

    	$this->assign('list',$res);
    	$about = db('gw_about')->where('id',$id)->find();
		$title='PHP工程师';
		if($id==7){
		$title='JAVA工程师';
		}else if($id==8){
		$title='前端工程师';
		}
		$this->assign('title',$title);
    	$this->assign('about',$about);
    	
    	$this->assign('id',$id);
    	return view();
    }
    
     //学院动态
    public function newlist($pid=4,$id='',$fid=1)
    {		
			$zhi='';
			$res = db('gw_news')->where('pid',$pid)->paginate(4, false, get_query());
		if($id){
			$zhi='123';
			$res = db('gw_news')->where('id',$id)->limit(1)->select();
		}
		$title='行业前景';
		if($pid==4){
		$title='校园动态';
		}
		$this->assign('zhi',$zhi);
		$this->assign('title',$title);
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',$fid)->select();
		$this->assign('type',$type);
		$this->assign('pid',$pid);
		
    	return $this->fetch();
    }
    public function newshow($id,$pid=4)
    {
    	$type = db('gw_newscate')->where('pid',1)->select();
    	$this->assign('type',$type);
    	
    	$data = db('gw_news')->where('id',$id)->find();
    	db('gw_news')->where('id',$id)->setInc('click',2);
    	$this->assign('data',$data);
    	$this->assign('pid',$pid);
    	return $this->fetch();
    }
    
     //学员就业
    public function joblist($id=6)
    {
    	$res = Db::name('gw_news')->where('pid',$id)->paginate(5, false, get_query());
		$title='学员作品';
		if($id==7){
		$title='学员活动';
		}
		$this->assign('title',$title);
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',2)->select();
		$this->assign('type',$type);
		$this->assign('pid',$id);
    	return $this->fetch();
    }
    public function jobshow($id,$pid=2)
    {
    	$type = db('gw_newscate')->where('pid',$pid)->select();
    	$this->assign('type',$type);
    	
    	$data = db('gw_news')->where('id',$id)->find();
    	db('gw_news')->where('id',$id)->setInc('click');
    	$this->assign('data',$data);
    	$this->assign('pid',$pid);
    	return $this->fetch();
    }

    //招生问答
    public function answerslist($pid=8,$fid=3){
		$title='校区问答';
		if($pid==9){
		$title='就业问答';
		}else if($pid==10){
		$title='课程问答';
		}
		$this->assign('title',$title);
    	$res = Db::name('gw_news')->where('pid',$pid)->paginate(5, false, get_query());
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',$fid)->select();
		$this->assign('type',$type);
		$this->assign('pid',$pid);
    	return $this->fetch();
    }
    public function answersshow($id,$fid=3)
    {
    	$type = db('gw_newscate')->where('pid',$fid)->select();
    	$this->assign('type',$type);
    	
    	$data = db('gw_news')->where('id',$id)->find();
    	db('gw_news')->where('id',$id)->setInc('click');
    	$this->assign('data',$data);
    	return $this->fetch();
    }
    
    
    public function noticeshow($id)
    {
    	$data = db('gw_message')->where('id',$id)->find();
    	$this->assign('data',$data);
    	return $this->fetch();
    }
    //在线留言
    public function message()
    {
    	
    	if($this->request->isPost()){
			$data = $this->request->post();
			$data['add_time'] = now_time();
			$res = db('gw_message')->insert($data);
			if($res){
				return $this->success('留言成功');
			}else{
				return $this->error('留言失败');
			}
		}else{
	    	return $this->fetch();
		}
    }
    
    //我要报名
    public function baoming()
    {
    	
    	if($this->request->isPost()){
			$data = $this->request->post();
			$data['add_time'] = now_time();
			$data['pid'] = 3;
			$res = db('gw_message')->insert($data);
			if($res){
				return $this->success('报名成功');
			}else{
				return $this->error('报名失败');
			}
		}else{
	    	return $this->fetch();
		}
    }
    
    
}
