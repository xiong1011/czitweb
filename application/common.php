<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
function get_query() {
	$param = request()->except(array('s'));
	$rst = array();
	foreach ($param as $k => $v) {
		if (!empty($v)) {
			$rst[$k] = $v;
		}
	}
	return array('query' => $rst);
}

/**
 * 返回格式化后的当前时间
 */
function now_time() {
	return date('Y-m-d H:i:s', time());
}

/**
 * 数组层级缩进转换
 * @param array $array
 * @param int   $pid
 * @param int   $level
 * @return array
 */
function array2level($array, $pid = 0, $level = 1) {
    static $list = [];
    foreach ($array as $v) {
        if ($v['pid'] == $pid) {
            $v['level'] = $level;
            $list[] = $v;
            array2level($array, $v['id'], $level + 1);
        }
    }

    return $list;
}
/**
 * 构建层级（树状）数组
 * @param array  $array 要进行处理的一维数组，经过该函数处理后，该数组自动转为树状数组
 * @param string $pid 父级ID的字段名
 * @param string $child_key_name 子元素键名
 * @return array|bool
 */
function arr2tree($tree, $rootId = 0,$level=1) {  
    $return = array();  
    foreach($tree as $leaf) {  
        if($leaf['pid'] == $rootId) {
            $leaf["level"] = $level;
            foreach($tree as $subleaf) {  
                if($subleaf['pid'] == $leaf['id']) {
                    $leaf['children'] = arr2tree($tree, $leaf['id'],$level+1);  
                    break;  
                }  
            } 
            $return[] = $leaf; 
        } 
    } 
    return $return;  
}

/******
 * 接收到数组 把值为空的元素删除
 * 注意：用于一维关联数组
 */
function delete_arr($array){
    $array=array_filter($array,create_function('$v','return !empty($v);'));
    return $array;
}

/*****  回家状态 ******/
function dengji_status($id = 1){
    switch($id) {
        case 1 :
            return '到家';
            break;
        default :
            return '没到家';
            break;
    }
}

/*****  奖罚 ******/
function puni($id = 1){
    switch($id) {
        case 1 :
            return '罚';
            break;
        default :
            return '奖';
            break;
    }
}
function puni_status($id = 1){
    switch($id) {
        case 1 :
            return '已处理';
            break;
        default :
            return '待处理';
            break;
    }
}


/******* 学生状态 ******/
function student_status($id = 1) {
	switch($id) {
		case 1 :
			return '报名';
			break;
		case 2 :
			return '学习中';
			break;
		case 3 :
			return '毕业';
			break;
		case 4 :
			return '实习中';
			break;
		default :
			return '上班';
			break;
	}
}
function teacher_status($id = 1) {
	switch($id) {
		case 1 :
			return '在职';
			break;
		default :
			return '离职';
			break;
	}
}

function admini_status($id = 1) {
	switch($id) {
		case 1 :
			return '正常';
			break;
		default :
			return '禁用';
			break;
	}
}

function km($id = 1) {
	switch($id) {
		case 1 :
			return 'PHP开发工程师';
			break;
		case 2 :
			return 'JAVA开发工程师';
			break;
		default :
			return '前端工程师';
			break;
	}
}
function gw_jj($id = 1) {
	switch($id) {
		case 1 :
			return '学校环境';
			break;
		case 2 :
			return '师资力量';
			break;
		case 3 :
			return '资深荣誉';
			break;
		default :
			return '校企合作';
			break;
	}
}