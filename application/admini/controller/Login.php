<?php
namespace app\admini\controller;
use think\Controller;
use think\captcha\Captcha;
use think\Session;

class Login extends Controller
{
    public function index()
    {
    	/*if(session('ADMIN')){
			$this->redirect('index/index');
		}*/
		
		if($this->request->isPost()){
			$data = input('post.');
			if(!captcha_check($data['verity'] )){
				return json(['code'=>2,'message'=>'验证不对，请重新输入']);
			}
			$res = db('auth_admin')->where('username',$data['username'])->find();
			if($res['password'] == md5($data['password'].config('auth_key'))){
				if ($res['status'] != 1) {
						return json(['code'=>4,'message'=>'当前用户已禁用']);
					} else {
						session('ADMIN', $res);
						db('auth_admin')->update(array(
							'last_login_time' => now_time(),
							'last_login_ip' => $this->request->ip(),
							'id' => $res['id'],
							'token' => md5($res['username'] . $res['password']) //更改登录状态
						));
						return json(['code'=>1,'url'=>'/admini/index','message'=>'登陆成功','aa'=>'21']);
					}
				
			}else{
				return json(['code'=>3,'message'=>'登陆失败']);
			}
		}else{
			return $this->fetch();	
		}
    }
    
    public function tuichu()
	{
		Session::clear();
		return json(['code'=>1,'url'=>url('login/index'),'message'=>'退出成功!']);
	}
}
