<?php
namespace app\admini\controller;
use think\Db;
use app\admini\controller\Base;

class Web extends Base
{
	public function about()
	{
		$res = db('gw_about')->field('content,add_time,keys',true)->where('pid',1)->select();
		$this->assign('data',$res);
		return $this->fetch();
	}
	public function about_updata()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			file_put_contents('test.txt',$data['content']);
			unset($data['file']);
			$res = db('gw_about')->update($data);
			if($res){
				if($data['pid'] == 1){
					return $this->toSuccess('/admini/web/about');
				}else{
					return $this->toSuccess('/admini/web/hotcourse');
				}
			}else{
				return $this->toError();
			}
		}else{
			$id = input('id');
			$res = db('gw_about')->where('id',$id)->find();
			$this->assign('data',$res);
			return $this->fetch();	
		}
		
	}
	
	//热门课程
	public function hotcourse()
	{
		$res = Db::name('gw_about')->where('pid',2)->select();
		$this->assign('data',$res);
		return $this->fetch();
	}
	
	
	//学院动态
	public function news()
	{
		$map = array();
		$keyword=input('keyword');
		$pid=input('pid');
		if ($keyword) {
			$map['a.title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($pid) {
			$map['a.pid'] = $pid;
		}else{
			$map['a.pid'] = array(
				'in',
				'4,5'
			);
		}
		$this->assign('pid', $pid);
		
		$res = Db::name('gw_news')->alias('a')->field('a.*,w.cate_name')->join('gw_newscate w','a.pid = w.id')->where($map)->order('add_time desc')->paginate(5, false, get_query());
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',1)->select();
		$this->assign('type',$type);
		return $this->fetch();
	}
/*	//批量删除
	public function news_plsc(){
			//批量删除
         $checkedList = $this->request->post('delitems');//接收ajax请求
		 $nub=explode(',',$checkedList);
		 //对于不同表处理
				$bm='gw_news';
		 if($this->request->post('sc')){
				$bm='gw_message';
		 }
		 if($this->request->post('gw_ads')){
				$bm='gw_ads';
		 }
		 if($this->request->post('gw_alist')){
				$bm='gw_alist';
		 }
		foreach($nub as $v){//循环删除
			   if(!db($bm)->where("id",$v)->delete()){
						 die('删除失败！！');
					}
		   }
					die('删除成功！！');
	}
	*/
	public function aa(){
		echo __FILE__;
	}
	public function news_add()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			$data['add_time'] = now_time();
			unset($data['file']);
			$fid = $data['fid'];
			unset($data['fid']);
			$res = db('gw_news')->insert($data);
			if($res){
				if($fid == 1){
					return $this->toSuccess('/admini/web/news');
				}else if($fid == 2){
					return $this->toSuccess('/admini/web/job');
				}else{
					return $this->toSuccess('/admini/web/answers');
				}
			}else{
				return $this->toError();
			}
		}else{
			$type = Db::name('gw_newscate')->where('pid',input('fid'))->select();
			$this->assign('type',$type);
			$this->assign('fid',input('fid'));
			return $this->fetch();	
		}
		
	}
	public function news_updata()
	{
		if($this->request->isPost()){
		$data = $this->request->post();
			$url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
			unset($data['file']);
			$pid=$data['p_id'];
			unset($data['p_id']);
			$res = db('gw_news')->update($data);
			if($res){
				if($pid== 1){
					return $this->Success('操作成功',"{$url}/index.php/admini/web/news");
				}else if($pid== 2){
					return $this->Success('操作成功',"{$url}/index.php/admini/web/job");
				}else{
					return $this->Success('操作成功',"{$url}/index.php/admini/web/answers");
				}

			}else{
				return $this->Success('操作失败');
			}
		}else{
			$data = db('gw_news')->find(input('id'));
			$this->assign('data',$data);
			$type = Db::name('gw_newscate')->where('pid',input('pid'))->select();
			$this->assign('type',$type);
			$this->assign('pid',input('pid'));
			return $this->fetch();	
		}
		
	}
	
	//学校简介
	public function alist()
	{
		$map = array();
		$keyword=input('keyword');
		$pid=input('pid');
		if ($keyword) {
			$map['title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($pid) {
			$map['pid'] = $pid;
		}
		$this->assign('pid', $pid);
		
		$res = Db::name('gw_alist')->where($map)->order('add_time desc')->paginate(5, false, get_query());
		$this->assign('data',$res);
		return $this->fetch();
	}
	
	public function alist_add()
	{
		$sj=Date('Y').Date('m').date('d');
		if($this->request->isPost()){
			$advimg=isset($_FILES['images'])?$_FILES['images']:'';
			$data = $this->request->post();
			$data['images']='/uploads/upload/'.$sj.'/'.$_FILES["images"]["name"];
			$data['add_time'] = now_time();
			unset($data['file']);
			$res = db('gw_alist')->insert($data);
				if($res){
				//文件路径
				$lujin="D:/amp/shuaige/bjq/xionlaoshi/uploads/upload/".$sj;

				//url路径 http://www.xionlaoshi.com/
				//$url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
				$url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
				if(file_exists($lujin)){
				if(move_uploaded_file($advimg['tmp_name'],$lujin.'/'.$advimg['name'])){
					return $this->Success('操作成功',"{$url}/index.php/admini/web/alist");
				}
					return $this->Success('操作失败');
				}

				is_dir($lujin) OR mkdir($lujin, 0777, true);
				if(move_uploaded_file($advimg['tmp_name'],$lujin.'/'.$advimg['name'])){
					return $this->Success('操作成功',"{$url}/index.php/admini/web/alist");
				}
					return $this->Success('操作失败');
				}else{
					return $this->Success('操作失败');
				}
		}else{
			return $this->fetch();	
		}
		
	}
	public function alist_updata()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			unset($data['file']);
			$res = db('gw_alist')->update($data);
			if($res){
				$url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
				return $this->Success('操作成功',"$url/index.php/admini/web/alist");
			}else{
				return $this->Success("操作失败");
			}
		}else{
			$data = db('gw_alist')->find(input('id'));
			$this->assign('data',$data);
			$this->assign('pid',input('pid'));
			return $this->fetch();	
		}
		
	}
	
	
	
	//学员就业
	public function job()
	{
		$map = array();
		$keyword=input('keyword');
		$pid=input('pid');
		if ($keyword) {
			$map['a.title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($pid) {
			$map['a.pid'] = $pid;
		}else{
			$map['a.pid'] = array(
				'in',
				'6,7'
			);
		}
		$this->assign('pid', $pid);
		
		$res = Db::name('gw_news')->alias('a')->field('a.*,w.cate_name')->join('gw_newscate w','a.pid = w.id')->where($map)->paginate(5, false, get_query());
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',2)->select();
		$this->assign('type',$type);
		return $this->fetch();
	}
	
	//招生问答
	public function answers()
	{
		$map = array();
		$keyword=input('keyword');
		$pid=input('pid');
		if ($keyword) {
			$map['a.title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($pid) {
			$map['a.pid'] = $pid;
		}else{
			$map['a.pid'] = array(
				'in',
				'8,9,10'
			);
		}
		$this->assign('pid', $pid);
		
		$res = Db::name('gw_news')->alias('a')->field('a.*,w.cate_name')->join('gw_newscate w','a.pid = w.id')->where($map)->paginate(5, false, get_query());
		$this->assign('data',$res);
		$type = Db::name('gw_newscate')->where('pid',3)->select();
		$this->assign('type',$type);
		return $this->fetch();
	}
	
	//通知公告
	public function message()
	{
		$map = array();
		$keyword=input('keyword');
		if ($keyword) {
			$map['title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		$map['pid'] = 1;
		$res = Db::name('gw_message')->where($map)->paginate(5, false, get_query());
		$this->assign('data',$res);
		return $this->fetch();
	}
	//在线报名
	public function baoming()
	{
		$map = array();
		$keyword=input('keyword');
		if ($keyword) {
			$map['title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		$map['pid'] = 3;
		$res = Db::name('gw_message')->where($map)->paginate(5, false, get_query());
		$this->assign('data',$res);
		return $this->fetch();
	}
	//通知
	public function notice()
	{
		$map = array();
		$keyword=input('keyword');
		if ($keyword) {
			$map['title'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		$map['pid'] = 2;
		$res = Db::name('gw_message')->where($map)->paginate(5, false, get_query());
		$this->assign('data',$res);
		return $this->fetch();
	}
	public function notice_add()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			$data['add_time'] = now_time();
			$data['pid']=2;
			unset($data['file']);
			$res = db('gw_message')->insert($data);
			$url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
			if($res){
				return $this->Success('操作成功',"{$url}/index.php/admini/web/notice");
			}else{
				return $this->toError();
			}
		}else{
			return $this->fetch();	
		}
		
	}
	public function notice_updata()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			unset($data['file']);
			$res = db('gw_message')->update($data);
			if($res){
				return $this->toSuccess('/admini/web/notice');
			}else{
				return $this->toError();
			}
		}else{
			$data = db('gw_message')->find(input('id'));
			$this->assign('data',$data);
			return $this->fetch();	
		}
		
	}
	
	//广告管理
	public function ads()
	{
		
		$res = Db::name('gw_ads')->order('sort asc')->select();
		$this->assign('data',$res);
		return $this->fetch();
	}
	
	public function ads_add()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			$res = db('gw_ads')->insert($data);
			if($res){
				return $this->toSuccess('/admini/web/ads');
			}else{
				return $this->toError();
			}
		}else{
			return $this->fetch();	
		}
		
	}
	public function ads_updata()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			unset($data['file']);
			$url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
			$res = db('gw_ads')->update($data);
			if($res){
				return $this->Success('操作成功',"{$url}/index.php/admini/web/ads");
			}else{
				return $this->Success('操作失败');
			}
		}else{
			$data = db('gw_ads')->find(input('id'));
			$this->assign('pid',input('pid'));
			$this->assign('data',$data);
			return $this->fetch();	
		}
		
	}
	
}
