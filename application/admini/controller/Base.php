<?php
namespace app\admini\controller;
use think\Controller;
use think\Db;
use think\request;
use think\Session;

class Base extends Controller
{
    public function _initialize()
    {
    	parent::_initialize();

        if(!session('ADMIN')){
			$this->redirect('login/index');
		}
        $this->assign('usename',session('ADMIN.username'));
//        $this->checkAuth();
        $this->getMenu();
		// 判断登录状态
		$user = db('auth_admin')->field('token')->where('id', session('ADMIN.id'))->find();
		if ($user['token'] != session('ADMIN.token')) {
			//session('ADMIN', null);
			//$this->success('该账号已在别处登录！', 'login/index');
		}
    }
    
    /**
	 * 权限检查
	 * @return bool
	 */
	protected function checkAuth() {
		$auth = new \com\Auth();   
        $module     = strtolower(request()->module());
        $controller = strtolower(request()->controller());
        $action     = strtolower(request()->action());
        $url        = $module."/".$controller."/".$action;

        //跳过检测以及主页权限
        if(session('ADMIN.id')!=1){
            if(!in_array($url, ['admini/index/index','admini/index/console','admini/admini/getJson'])){
                if(!$auth->check($url,session('ADMIN.id'))){
                    $this->success('抱歉，您没有操作权限！','index/console');
                }
            }
        }
	}

	protected function getMenu() {
		$res = db('auth_rule')->order('sort asc')->where('status', 1)->select();
		$this->assign('menu',arr2tree($res));
	}
	
	//上传图片
	 function upload(){
	    // 获取表单上传文件 例如上传了001.jpg
	    $file = Request::instance()->file('file');
	    $info = $file->validate(['size'=>1567800,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'uploads' . DS . 'upload');
	    if($info){
            $infos = $info->getInfo();
            $name = $infos['name'];
            $name_path =str_replace('\\',"/",$info->getSaveName());
            //成功上传后 获取上传信息
            $result["code"] = '0';
            $result["msg"] = "上传成功";
            $result['data']["src"] = "/uploads/upload/".$name_path;
            $result['data']["title"] = $name;
        }else{
            // 上传失败获取错误信息
            $result["code"] = "2";
            $result["msg"] = "上传出错";
            $result['data']["src"] ='';
        }
        return json($result);
	}
	//编辑器
	 function uploads(){
		$file = Request::instance()->file('file');
        $info = $file->validate(['size'=>1567800,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'uploads' . DS . 'edit' );
        if($info){
            $infos = $info->getInfo();
            $name = $infos['name'];
            $name_path =str_replace('\\',"/",$info->getSaveName());
            //成功上传后 获取上传信息
            $result["code"] = '0';
            $result["msg"] = "上传成功";
            $result['data']["src"] = "/uploads/edit/".$name_path;
            $result['data']["title"] = $name;
        }else{
            // 上传失败获取错误信息
            $result["code"] = "2";
            $result["msg"] = "上传出错";
            $result['data']["src"] ='';
        }
        return json($result);
	}
	
	//公用删除
	 function dell()
	{
		$ids = input('param.ids/a') ? input('param.ids/a') : input('param.id/a');
		$table = input('param.table', strtolower(request()->action()));
		$res = db($table)->where('id', 'in', $ids)->delete();
		if($res){
			$this->success('删除成功');
		}else{
			$this->error('删除失败');
		}
		
	}
	//通用排序
	 function sorts() {
		$table = input('param.table', strtolower(request()->controller()));
		$field = input('param.field/s', 'sort');
		$ids = input('param.ids/d') ? input('param.ids/d') : input('param.id/d');
		$val = input('param.val/d', 0);
		$res = db($table)->where('id', 'in', $ids)->setField($field, $val);
		if ($res === false) {
			$this->error('排序失败');
		}
		$this->success('排序成功');
	}
	/**
	 * 状态设置
	 * @return mixed
	 */
	public function status() {
		$ids = input('param.ids/a') ? input('param.ids/a') : input('param.id/a');
		$table = input('param.table', strtolower(request()->controller()));
		if (in_array(1, $ids)) {
			$this->error('默认管理员组无法设置');
		}
		$val = input('param.val');
		if ($val == false) {
			$val = 2;
		} else {
			$val = 1;
		}
		$res = db($table)->where('id', 'in', $ids)->setField('status', $val);
		if ($res === false) {
			$this->error('设置失败');
		}
		$this->success('设置成功');
	}
	/**
	 * 返回成功带提示信息
	 */
	 function toSuccess($url = '') {
		$json['code'] = 1;
		$json['url'] = $url;
		$json['message'] = '操作成功';
		return json($json);
	}
	
	/**
	 * 返回失败带提示信息
	 */
	 function toError() {
		$json['code'] = 2;
		$json['message'] = '操作失败';
		return json($json);
	}
	
	
}