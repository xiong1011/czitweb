<?php
namespace app\admini\controller;
use think\Controller;
use think\Db;
use think\request;
use think\Session;

use app\admini\controller\Base;

class Admini extends Base
{
	
	//用户管理
    public function admini($keyword='')
    {
    	$map = array();
		if ($keyword) {
			$map['u.login_name'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		
		$res = db('auth_admin u')->field('u.*,g.title')->join('auth_group_access a', 'u.id=a.uid')->join('auth_group g', 'a.group_id=g.id')->where($map)->order('id desc')->paginate();
		$this->assign('data',$res);
		return $this->fetch();
    }
    
    public function admini_add()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			
			$admin['login_name'] = $data['login_name'];
			$admin['login_pwd'] = md5($data['login_pwd'].'czit');
			$admin['status'] = $data['status'];
			$admin['add_time'] = now_time();
			
			$uid = db('auth_admin')->insertGetId($admin);
			$group['group_id'] = $data['group_id'];
			if($uid){
				$group['uid'] = $uid;
				db('auth_group_access')->insert($group);
				return $this->toSuccess('/admini/admini/admini');
			}else{
				return $this->toError();
			}
		}else{
			$group = Db::name('auth_group')->select();
			$this->assign('group',$group);
			return $this->fetch();	
		}
		
	}
    
    public function admini_updata()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			
			$admin['login_name'] = $data['login_name'];
			if($data['login_pwd']){
				$admin['login_pwd'] = md5($data['login_pwd'].'czit');
			}
			$admin['status'] = $data['status'];
			
			$uid = db('auth_admin')->update($admin);
			$group['group_id'] = $data['group_id'];
			if($uid){
				$group['uid'] = $data['id'];
				db('auth_group_access')->update($group);
				return $this->toSuccess('/admini/admini/admini');
			}else{
				return $this->toError();
			}
		}else{
			$group = Db::name('auth_group')->select();
			$this->assign('group',$group);
			$res = db('admini u')->field('u.*,g.id group_id')->join('auth_group_access a', 'u.id=a.uid')->join('auth_group g', 'a.group_id=g.id')->where('u.id',input('id'))->find();
			$this->assign('data',$res);
			return $this->fetch();	
		}
		
	}
    
    //规则
    public function rule($keyword='')
    {
		$map = array();
		if ($keyword) {
			$map['u.login_name'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		$res = Db::name('auth_rule')->order('sort asc')->where($map)->select();
		//dump(array2level($res));
		$this->assign('data',array2level($res));
		return $this->fetch();
    }
	
	public function rule_add($pid='0')
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			$res= db('auth_rule')->insert($data);
			if($res){
				return $this->toSuccess('/admini/admini/rule');
			}else{
				return $this->toError();
			}
		}else{
			$res = Db::name('auth_rule')->order('id desc')->select();
			$this->assign('data',array2level($res));
			$this->assign('pid',$pid);
			return $this->fetch();	
		}
		
	}
	public function rule_updata($id='')
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			$res= db('auth_rule')->update($data);
			if($res){
				return $this->toSuccess('/admini/admini/rule');
			}else{
				return $this->toError();
			}
		}else{
			$res = Db::name('auth_rule')->order('id desc')->select();
			$this->assign('type',array2level($res));
			$ress = Db::name('auth_rule')->find($id);
			$this->assign('data',$ress);
			return $this->fetch();	
		}
		
	}
	
	 public function group()
    {
		
		$res = Db::name('auth_group')->order('id desc')->paginate(5, false, get_query());
		$this->assign('data',$res);
		return $this->fetch();
    }
	
	public function group_add($pid='0')
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			if($data['ids']){
				$data['rules']=implode(",", $data['ids']);
			}
			unset($data['ids']);
			$res= db('auth_group')->insert($data);
			if($res){
				return $this->success('新增成功','/admini/admini/group');
			}else{
				return $this->toError();
			}
		}else{
			return $this->fetch();	
		}
		
	}
	public function group_updata($id='')
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			if($data['ids']){
				$data['rules']=implode(",", $data['ids']);
			}
			unset($data['ids']);
			$res= db('auth_group')->update($data);
			if($res){
				return $this->success('修改成功','/admini/admini/group');
			}else{
				return $this->toError();
			}
		}else{
			$data = Db::name('auth_group')->find($id);
			$this->assign('data',$data);
			return $this->fetch();	
		}
		
	}

	

	/**
	 * 删除菜单权限
	 * @param $id
	 */
	public function ruledelete() {
		$ids = input('param.ids/a') ? input('param.ids/a') : input('param.id/a');
		$table = input('param.table', strtolower(request()->action()));
		// 判断组下面
		$access = db($table)->where('pid', 'in', $ids)->find();
		if ($access) {
			$this->error('组下子菜单！请先删除父级菜单再试！');
		}
		// 获取主键
		$res = db($table)->where('id', 'in', $ids)->delete();
		if ($res === false) {
			$this->error('删除失败');
		}
		$this->success('删除成功');
	}
	
	
	/**
	 * 更新权限组规则
	 * @param $id
	 * @param $auth_rule_ids
	 */
	public function auth($id, $auth_rule_ids = '') {
		if ($this->request->isPost()) {
			if ($id) {
				$group_data['id'] = $id;
				$group_data['rules'] = is_array($auth_rule_ids) ? implode(',', $auth_rule_ids) : '';

				if (db('auth_group')->update($group_data, $id) !== false) {
					$this->success('授权成功');
				} else {
					$this->error('授权失败');
				}
			}
		} else {
			return $this->fetch('auth', array('id' => $id));
		}
	}

	/**
	 * AJAX获取规则数据
	 * @param $id
	 * @return mixed
	 */
	public function getJson($id) {
		$auth_group_data = db('auth_group')->find($id);
		$auth_rules = explode(',', $auth_group_data['rules']);
		$auth_rule_list = db('auth_rule')->field('id,pid,name as title')->select();
		foreach ($auth_rule_list as $key => $value) {
			in_array($value['id'], $auth_rules) && $auth_rule_list[$key]['checked'] = true;
		}
		return $auth_rule_list;
	}

}
