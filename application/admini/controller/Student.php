<?php
namespace app\admini\controller;
use app\admini\controller\Base;
use think\Db;
class Student extends Base
{
    /**** 渲染学生信息 ********/
    public function index($keyword='',$tel='',$grade='')
    {
    	$map = array();
		if ($keyword) {
			$map['a.name'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($tel) {
			$map['a.tel'] = $tel;
		}
		$this->assign('tel', $tel);
		if ($grade) {
			$map['a.classid'] = $grade;
		}
		$this->assign('grade', $grade);
		
    	$res = db('oa_student')
            ->alias('a')
            ->field('a.*,w.class_name')
            ->join('oa_classname w','a.classid = w.id')
            ->where($map)->order('a.id desc')
            ->paginate(5, false, get_query());
    	$this->assign('data',$res);
    	$type = Db::name('oa_classname')->select();
		$this->assign('type',array2level($type));
		return $this->fetch();
    }

    /**** 添加学生信息 ********/
	public function add_stu()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			unset($data['file']);
			$data['add_time']=now_time();//admini

			$res = db('oa_student')->insert($data);
			if($res){
				return $this->toSuccess('/admini/student/index');
			}else{
				return $this->toError();
			}
		}else{
			$res = db('oa_classname')->select();
			$this->assign('type',array2level($res));
			return $this->fetch();	
		}
		
	}

	/**** 修改学生信息 ********/
	public function updata_stu()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			unset($data['file']);
			$res = db('oa_student')->update($data);
			if($res){
				return $this->toSuccess('/admini/student/index');
			}else{
				return $this->toError();
			}
		}else{
			$res = db('oa_student')->find(input('id'));
			$this->assign('data',$res);
			$res = db('oa_classname')->select();
			$this->assign('type',array2level($res));
			return $this->fetch();	
		}
		
	}


	/******  显示学生列表  ******/
	public function xuefen($keyword='',$tel='',$grade='')
    {
    	$map = array();
		if ($keyword) {
			$map['a.name'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($tel) {
			$map['a.tel'] = $tel;
		}
		$this->assign('tel', $tel);
		if ($grade) {
			$map['a.grade'] = $grade;
		}
		$this->assign('grade', $grade);
		
    	$res = db('oa_student')->alias('a')->field('a.*,w.class_name')->join('oa_classname w','a.grade = w.id')->where($map)->order('a.add_time desc')->paginate(5, false, get_query());
    	$this->assign('data',$res);
    	$type = Db::name('oa_classname')->select();
		$this->assign('type',array2level($type));
		return $this->fetch();
    }

    /**** 奖惩 ********/
	public function jiangcheng($keyword='',$tel='',$jf_status='')
    {
    	$map = array();
    	$map['a.jf_status']=2;  //查询状态为 2待处理
		if ($keyword) {
			$map['s.name'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($tel) {
			$map['s.tel'] = $tel;
		}
		$this->assign('tel', $tel);
		if ($jf_status) {
			$map['a.jf_status'] = $jf_status;
		}
		$this->assign('grade', $jf_status);
		
    	$res = db('oa_student_jiangfa')
            ->alias('a')
            ->join('oa_student s','a.student_id = s.id')
            ->join('oa_classname w','s.classid = w.id')
            ->where($map)
            ->order('a.id desc')
            ->field('a.*,w.class_name,s.name,s.tel,s.integral,s.images')
            ->paginate(5, false, get_query());
    	$this->assign('data',$res);
    	$type = Db::name('oa_classname')->select();
		$this->assign('type',array2level($type));
		return $this->fetch();
    }

    /******  添加奖罚  ******/
    public function add_jiangcheng(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $show = db('oa_student')->field('name,id,classid')->where('name',$data['name'])->find(); /*** 判断学生是否存在 ****/
            unset($data['name']);
            $data['student_id']=$show['id'];
            $data['student_classid']=$show['classid'];
            $data['puni_time']=time();
            if($show){
                $res = db('oa_student_jiangfa')->insert($data);
                if($res){
                    return $this->toSuccess('/admini/student/jiangcheng');
                }else{
                    return $this->toError();
                }
            }else{
                return json(['code'=>-2,'message'=>'没有找到这个学生']);
            }

        }else{
            return $this->fetch();
        }
    }

    /**** 奖惩修改 ********/
    public function updata_jiangcheng($id=''){
        if($this->request->isPost()){
            $data = $this->request->post();
            if($data['puni_status']==1){ //修改学分
                $up = db('oa_student')->where("id",$data['student_id'])->setDec('integral',$data['number']);
            }else{
                $up = db('oa_student')->where("id",$data['student_id'])->setInc('integral',$data['number']);
            }

            $array = delete_arr($data);  //将数组里的空元素删除
            $array['jf_status']=1;  //状态为1 已处理
            if($up){
                $res = db('oa_student_jiangfa')->update($array);
                if($res){
                    return $this->toSuccess('/admini/student/jiangcheng');
                }else{
                    return $this->toError();
                }
            }
        }else{
            $res = db('oa_student_jiangfa')
                ->alias('a')
                ->field('a.*,w.class_name,s.name,s.tel,s.images')
                ->join('oa_student s','a.student_id = s.id')
                ->join('oa_classname w','s.classid = w.id')
                ->where('a.id',$id)->find();
            $this->assign('data',$res);
            return $this->fetch();
        }
    }


    
    /**** 回家登记 ********/
	public function dengji($keyword='',$tel='',$classid='')
    {
    	$map = array();
		if ($keyword) {
			$map['s.name'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($tel) {
			$map['s.tel'] = $tel;
		}
		$this->assign('tel', $tel);
		if ($classid) {
			$map['s.classid'] = $classid;
		}
		$this->assign('grade', $classid);

    	$res = db('oa_student_dengji')
            ->alias('a')
            ->join('oa_student s','a.student_id = s.id')
            ->join('oa_classname w','s.classid = w.id')
            ->where($map)->order('a.add_time desc')
            ->field('a.*,w.class_name,s.name,s.tel,s.images')
            ->paginate(5, false, get_query());
    	$this->assign('data',$res);
    	$type = Db::name('oa_classname')->select();
		$this->assign('type',array2level($type));
		return $this->fetch();
    }

    /**** 修改回家登记 ********/
    public function updata_dengji($id=''){
        if($this->request->isPost()){
            $data = $this->request->post();
            $res = db('oa_student_dengji')->update($data);
            if($res){
                return $this->toSuccess('/admini/student/dengji');
            }else{
                return $this->toError();
            }
        }else{
            $res = db('oa_student_dengji')
                ->alias('a')
                ->join('oa_student s','a.student_id = s.id')
                ->join('oa_classname w','s.classid = w.id')
                ->where('a.id',$id)->find();
            $this->assign('data',$res);
            return $this->fetch();
        }
    }

    /******  成绩列表  *******/
    public function chengji($keyword='',$course='',$classid=''){
        $map = array();
        $order='a.id';
        if ($keyword) {
            $map['s.name'] = array(
                'like',
                '%' . $keyword . '%'
            );
        }
        $this->assign('keyword', $keyword);
        if ($course) {
            $order = $course;
        }
        $this->assign('course', $course);
        if ($classid) {
            $map['s.classid'] = $classid;
        }
        $this->assign('grade', $classid);

        $res = db('oa_student_chengji')
            ->alias('a')
            ->join('oa_student s','a.student_id = s.id')
            ->join('oa_classname w','s.classid = w.id')
            ->where($map)->order($order,"desc")
            ->field('a.*,w.class_name,s.name')
            ->paginate(5, false, get_query());
        $this->assign('data',$res);
        $type = Db::name('oa_classname')->select();
        $chengji = Db::name('oa_student_chengji')->select();
        $this->assign('chengji',$chengji);
        $this->assign('type',array2level($type));
        return $this->fetch();
    }

    /****** 修改成绩 ******/
    public function updata_chengji($id=''){
        if($this->request->isPost()){
            $data = $this->request->post();
            $res = db('oa_student_chengji')->update($data);
            if($res){
                return $this->toSuccess('/admini/student/chengji');
            }else{
                return $this->toError();
            }
        }else{
            $res = db('oa_student_chengji')
                ->alias('a')
                ->join('oa_student s','a.student_id = s.id')
                ->field('a.*,s.name,s.images')
                ->where('a.id',$id)->find();
            $this->assign('data',$res);
            return $this->fetch();
        }
    }

    /********  添加成绩 ***********/
    public function add_chengji(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $id = db('oa_student')->where('name',$data['name'])->field('id')->find();
            if($id){
                $data['student_id']=$id['id'];
                unset($data['name']);
                $res = db('oa_student_chengji')->insert($data);
                if($res){
                    return $this->toSuccess('/admini/student/chengji');
                }else{
                    return json(['code'=>-2,'msg'=>'添加失败']);
                }
            }else{
                return json(['code'=>-2,'message'=>'没有找到这个学生 请在学生列表添加该学生']);
            }
        }else{
            return $this->fetch();
        }
    }


    /***   学费列表 *****/
    public function xuefei($keyword='')
    {
        $map = array();
        if ($keyword) {
            $map['s.name'] = array(
                'like',
                '%' . $keyword . '%'
            );
        }
        $this->assign('keyword', $keyword);

        $res = db('oa_student_xuefei')
            ->alias('a')
            ->join('oa_student s','a.student_id = s.id')
            ->where($map)->order('a.id desc')
            ->field('a.*,s.name')
            ->paginate(5, false, get_query());
        $this->assign('data',$res);
        return $this->fetch();
    }

    /*****  学费修改 *****/
    public function updata_xuefei($id=''){
        if($this->request->isPost()){
            $data = $this->request->post();
            $id = db('oa_student')->where('name',$data['name'])->field('id')->find();
            if($id) {
                $data['student_id']=$id['id'];
                unset($data['name']);
                $res = db('oa_student_xuefei')->update($data);
                if($res){
                    return $this->toSuccess('/admini/student/xuefei');
                }else{
                    return $this->toError();
                }
            }else{
                return json(['code'=>-2,'message'=>'没有找到这个学生 请在学生列表添加该学生']);
            }
        }else{
            $res = db('oa_student_xuefei')
                ->alias('a')
                ->join('oa_student s','a.student_id = s.id')
                ->field('a.*,s.name')
                ->where('a.id',$id)->find();
            $this->assign('data',$res);
            return $this->fetch();
        }
    }

    /**** 添加学费信息  ****/
    public function add_xuefei(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $id = db('oa_student')->where('name',$data['name'])->field('id')->find();
            if ($id){
                unset($data['name']);
                $data['pay_time']=time();
                $data['student_id']=$id['id'];
                $res = db('oa_student_xuefei')->insert($data);
                if($res){
                    return $this->toSuccess('/admini/student/xuefei');
                }else{
                    return $this->toError();
                }
            }else{
                return json(['code'=>-2,'message'=>'没有找到这个学生 请在学生列表添加该学生']);
            }
        }else{
            return $this->fetch();
        }
    }


    /****  评价列表  *****/
    public function pingjia($keyword='',$classid=''){
        $map = array();
        if ($keyword) {
            $map['s.name'] = array(
                'like',
                '%' . $keyword . '%'
            );
        }
        $this->assign('keyword', $keyword);
        if ($classid) {
            $map['s.classid'] = $classid;
        }
        $this->assign('grade', $classid);

        $res = db('oa_student_pingjia')
            ->alias('a')
            ->join('oa_student s','a.student_id = s.id')
            ->join('oa_classname w','s.classid = w.id')
            ->where($map)->order('a.add_time desc')
            ->field('a.*,w.class_name,s.name')
            ->paginate(5, false, get_query());
        $this->assign('data',$res);
        $type = Db::name('oa_classname')->select();
        $this->assign('type',array2level($type));
        return $this->fetch();
    }

    /**** 添加评价 *****/
    public function add_pingjia(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $id = db('oa_student')->where('name',$data['name'])->field('id')->find();
            if ($id){
                unset($data['name']);
                $data['add_time']=time();
                $data['student_id']=$id['id'];
                $res = db('oa_student_pingjia')->insert($data);
                if($res){
                    return $this->toSuccess('/admini/student/pingjia');
                }else{
                    return $this->toError();
                }
            }else{
                return json(['code'=>-2,'message'=>'没有找到这个学生 请在学生列表添加该学生']);
            }
        }else{
            return $this->fetch();
        }
    }

    /*****  修改评价 *****/
    public function updata_pingjia($id=''){
        if($this->request->isPost()){
            $data = $this->request->post();
            $id = db('oa_student')->where('name',$data['name'])->field('id')->find();
            if($id) {
                $data['student_id']=$id['id'];
                unset($data['name']);
                $res = db('oa_student_pingjia')->update($data);
                if($res){
                    return $this->toSuccess('/admini/student/pingjia');
                }else{
                    return $this->toError();
                }
            }else{
                return json(['code'=>-2,'message'=>'没有找到这个学生 请在学生列表添加该学生']);
            }
        }else{
            $res = db('oa_student_pingjia')
                ->alias('a')
                ->join('oa_student s','a.student_id = s.id')
                ->field('a.*,s.name,s.images')
                ->where('a.id',$id)->find();
            $this->assign('data',$res);
            return $this->fetch();
        }
    }



	//导出
	public function excel_dc(){
 		//导入相关文件
//		require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/PHPExcel/PHPExcel.php';
//		require './vendor/PHPExcel/PHPExcel.php';
		//方法一
		vendor("PHPExcel.PHPExcel");   
		//实例化
		$phpexcel = new \PHPExcel();
		//设置比标题
		$phpexcel->getActiveSheet()->setTitle('学生信息');
		//设置表头
		$phpexcel->getActiveSheet() ->setCellValue('A1','姓名')
		                            ->setCellValue('B1','性别')
		                            ->setCellValue('C1','电话')
		                            ->setCellValue('D1','家长电话')
		                            ->setCellValue('E1','地址')
		                            ->setCellValue('F1','出生年月')
		                            ->setCellValue('G1','班级')
		                            ->setCellValue('H1','入校时间');
		//从数据库取得需要导出的数据
		$list = db('oa_student')->alias('a')
		->field('a.*,w.class_name')
		->join('oa_classname w','a.grade = w.id')
		->order('a.add_time desc')->select();
		//用foreach从第二行开始写数据，因为第一行是表头
		$i=2;
		foreach($list as $val){
			
   	 		$phpexcel->getActiveSheet() ->setCellValue('A'.$i,' '.$val['name'])
                            ->setCellValue('B'.$i, $val['sex'])
                            ->setCellValue('C'.$i, $val['username'])
                            ->setCellValue('D'.$i, $val['tel'])
                            ->setCellValue('E'.$i, $val['address'])
                            ->setCellValue('F'.$i, $val['age'])
                            ->setCellValue('G'.$i, $val['class_name'])
                            ->setCellValue('H'.$i, $val['in_time']);
		    $i++;
		}
		
		$obj_Writer = \PHPExcel_IOFactory::createWriter($phpexcel,'Excel5');
		$filename ='学生信息'. date('Y-m-d').".xls";//文件名
		
//		$obj_Writer->save($filename);
		
		//设置header
		header("Content-Type: application/force-download"); 
		header("Content-Type: application/octet-stream"); 
		header("Content-Type: application/download"); 
		header('Content-Disposition:inline;filename="'.$filename.'"'); 
		header("Content-Transfer-Encoding: binary"); 
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header("Pragma: no-cache"); 
		$obj_Writer->save('php://output');//输出		
		die();//种植执行
 	}
	
	/**
	 * 导入信息
	 * @param $id
	 */
	public function export_xl() {
		
		return $this->fetch();
	}
	
	public function saveImport(){  
        //import('phpexcel.PHPExcel', EXTEND_PATH);//方法二  
        vendor("PHPExcel.PHPExcel"); //方法一  
        $objPHPExcel = new \PHPExcel();  
  
        //获取表单上传文件  
        $file = request()->file('excel');  
        $info = $file->validate(['ext'=>'xlsx,xls,csv'])->move(ROOT_PATH . 'public' . DS . 'excel');  
        if($info){  
            $exclePath = $info->getSaveName();  //获取文件名  
            $file_name = ROOT_PATH . 'public' . DS . 'excel' . DS . $exclePath;   //上传文件的地址  
            $objReader =\PHPExcel_IOFactory::createReader('Excel5');  
            $obj_PHPExcel =$objReader->load($file_name, $encode = 'utf-8');  //加载文件内容,编码utf-8  
           // echo "<pre>";  
            $excel_array=$obj_PHPExcel->getsheet(0)->toArray();   //转换为数组格式  
            array_shift($excel_array);  //删除第一个数组(标题);  
            $data = [];  
            $i=0;  
           
            foreach($excel_array as $k=>$v) {  
            	if($v[0]){
            	$data[$k]['cate_id1'] = $v[0]; 
                $data[$k]['cate_id2'] = $v[1];
                $data[$k]['title'] = $v[2];
                $data[$k]['market'] = $v[3];
                $data[$k]['maxprice'] = $v[4]; 
                $data[$k]['minprice'] = $v[5]; 
                $data[$k]['pprice'] = $v[6]; 
                $data[$k]['rose'] = $v[7]; 
                $data[$k]['add_time'] = $v[8];	
                $data[$k]['update_time'] = now_time();
            	}
                $i++;  
            }  
            //dump($data)
           $success=db('newsjg')->insertAll($data); //批量插入数据  
           //$i=  
           $error=$i-$success;  
            echo "总{$i}条，成功{$success}条，失败{$error}条。";  
           // Db::name('t_station')->insertAll($city); //批量插入数据  
        }else{  
            // 上传失败获取错误信息  
            echo $file->getError();  
        }  
  
    }

    /*** 测试 **/
    public function test(){
        return "m is test";
    }

    /***
     * 好事要留名： 饶晋园 主要负责.学生管理,老师管理 2019.4.26 完成
     * 联系QQ：2556422181
     */

}
