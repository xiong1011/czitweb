<?php
namespace app\admini\controller;
use app\admini\controller\Base;
use think\Db;
class Teacher extends Base
{
	/******  老师列表  ******/
    public function index($keyword='',$tel='')
    {
    	$map = array();
		if ($keyword) {
			$map['a.teacher_name'] = array(
				'like',
				'%' . $keyword . '%'
			);
		}
		$this->assign('keyword', $keyword);
		if ($tel) {
			$map['a.tel'] = $tel;
		}
		$this->assign('tel', $tel);
		
    	$res = db('oa_teacher')->alias('a')->field('a.*,w.class_name')->join('oa_classname w','a.classid = w.id')->where($map)->order('a.add_time desc')->paginate(5, false, get_query());
    	$this->assign('data',$res);
        $type = Db::name('oa_classname')->select();
		$this->assign('type',array2level($type));
		return $this->fetch();
    }

    /******  添加老师 ******/
	public function add_tea()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			unset($data['file']);
			$data['add_time']=now_time();//admini
			$res = db('oa_teacher')->insert($data);
			if($res){
				return $this->toSuccess('/admini/teacher/index');
			}else{
				return $this->toError();
			}
		}else{
			$res = db('oa_classname')->select();
			$this->assign('type',array2level($res));
			return $this->fetch();	
		}
		
	}

    /******  修改老师 ******/
	public function updata_tea()
	{
		if($this->request->isPost()){
			$data = $this->request->post();
			unset($data['file']);
			$res = db('oa_teacher')->update($data);
			if($res){
				return $this->toSuccess('/admini/teacher/index');
			}else{
				return $this->toError();
			}
		}else{
			$res = db('oa_teacher')->find(input('id'));
			$this->assign('data',$res);
			$res = db('oa_classname')->select();
			$this->assign('type',array2level($res));
			return $this->fetch();	
		}
	}


    /******  课程列表  ******/
    public function kecheng($class_name='',$title=''){
        $map = array();
        if ($title) {
            $map['a.title'] = array(
                'like',
                '%' . $title . '%'
            );
        }
        $this->assign('title',$title);
        if ($class_name) {
            $map['w.class_name'] = $class_name;
        }
        $this->assign('class_name', $class_name);

        $res = db('oa_teacher_kecheng')
            ->alias('a')
            ->field('a.*,w.class_name')
            ->join('oa_kecheng_class w','a.kecheng_classid = w.id')
            ->where($map)->order('a.id desc')
            ->paginate(5, false, get_query());
        $this->assign('data',$res);
        $type = Db::name('oa_kecheng_class')->select();
        $this->assign('type',$type);
        return $this->fetch();
    }

    /***** 添加课程 ********/
    public function add_kecheng(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $rest = db('oa_teacher_kecheng')->find($data['title']);
            if($rest){
                return json(['code'=>-2,'message'=>'标题重复']);
            }else{
                $res = db('oa_teacher_kecheng')->insert($data);
                if($res){
                    return $this->toSuccess('/admini/teacher/kecheng');
                }else{
                    return $this->toError();
                }
            }
        }else{
            $res = db('oa_kecheng_class')->select();
            $this->assign('type',$res);
            return $this->fetch();
        }
    }

    /***  修改课程 ****/
    public function updata_kecheng($id=''){
        if($this->request->isPost()){
            $data = $this->request->post();
            $res = db('oa_teacher_kecheng')->update($data);
            if($res){
                return $this->toSuccess('/admini/teacher/kecheng');
            }else{
                return $this->toError();
            }
        }else{
            $res = db('oa_teacher_kecheng')->find(input('id'));
            $this->assign('data',$res);
            $res = db('oa_kecheng_class')->select();
            $this->assign('type',$res);
            return $this->fetch();
        }
    }

	/*****  进度 *****/
    public function jindu($class_name='PHP',$title=''){
        $map = array();
        if ($title) {
            $map['a.title'] = array(
                'like',
                '%' . $title . '%'
            );
        }
        $this->assign('title',$title);
        if ($class_name) {
            $map['w.class_name'] = $class_name;
        }
        $this->assign('class_name', $class_name);

        $res = db('oa_teacher_kecheng')
            ->alias('a')
            ->field('a.*,w.class_name')
            ->join('oa_kecheng_class w','a.kecheng_classid = w.id')
            ->where($map)->group('w.class_name')->order('a.id desc')
            ->paginate(20, false, get_query());
        $this->assign('data',$res);
        $type = Db::name('oa_kecheng_class')->select();

        $this->assign('type',$type);
        return $this->fetch();
    }

    /****** 添加进度  ******/
    public function add_jindu(){
        $data = $this->request->post();
        $where = array(
            'kecheng_id'=>$data['kecheng_id'],
            'teacher_id'=>session('ADMIN.id')
        );
        $teacher = db('oa_teacher_jindu')->where($where)->find(); //判断老师重复添加进度
        if($teacher){
            return json(['code'=>-2,'message'=>'课程进度重复']);
            die;
        }

        $data['teacher_id'] = session('ADMIN.id');
        $res = db('oa_teacher_jindu')->insert($data);
        if($res){
            return $this->toSuccess('/admini/teacher/jindu');
        }else{
            return $this->toError();
        }
    }

    /***** 进度记录  ****/
    public function jindujl($class_name='',$title=''){
        $map = array();
        $map['a.teacher_id']=session('ADMIN.id');  //查询跟老师相关的内容
       if ($title) {
            $map['w.title'] = array(
                'like',
                '%' . $title . '%'
            );
        }
        $this->assign('title',$title);
        if ($class_name) {
            $map['b.class_name'] = $class_name;
        }
        $this->assign('class_name', $class_name);

        $res = db('oa_teacher_jindu')
            ->alias('a')
            ->field('a.*,b.class_name,w.end_time,w.kecheng_text,w.title')
            ->join('oa_teacher_kecheng w','a.kecheng_id = w.id')
            ->join('oa_kecheng_class b','b.id = w.kecheng_classid')
            ->where($map)->order('a.id desc')
            ->paginate(20, false, get_query());
        $this->assign('data',$res);
        $type = Db::name('oa_kecheng_class')->select();

        $this->assign('type',$type);
        return $this->fetch();
    }

    /****** 修改进度记录  ******/
    public function update_jindu(){
        $data = $this->request->post();
        $res = db('oa_teacher_jindu')->update($data);
        if($res){
            return $this->toSuccess('/admini/teacher/jindujl');
        }else{
            return $this->toError();
        }

    }

    /*****  回显进度  *****/
    public function hui_jindu(){
        $res = db('oa_teacher_jindu')->find(input('id'));
        if ($res){
            return json(['code'=>1,'msg'=>$res]);
        }
    }




   /***
    * 好事要留名： 饶晋园 主要负责.学生管理,老师管理 2019.4.26 完成
    * 联系QQ：2556422181
    */
}
