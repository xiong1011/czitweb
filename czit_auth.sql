/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : auth

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-02-27 19:30:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `czit_auth_admin`
-- ----------------------------
DROP TABLE IF EXISTS `czit_auth_admin`;
CREATE TABLE `czit_auth_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_bin DEFAULT '' COMMENT '用户名',
  `password` varchar(32) COLLATE utf8_bin DEFAULT '' COMMENT '密码',
  `loginnum` int(11) DEFAULT '0' COMMENT '登陆次数',
  `last_login_ip` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '最后登录IP',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后登录时间',
  `status` int(1) DEFAULT '0' COMMENT '状态',
  `groupid` int(11) DEFAULT '1' COMMENT '用户角色id',
  `token` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of czit_auth_admin
-- ----------------------------
INSERT INTO `czit_auth_admin` VALUES ('1', 'admin', 'd23735424a541d8f7b1e25a58b6b408b', '290', '127.0.0.1', '2019', '1', '1', '01a40648b152d9c22c455fc2572b2c0f', null);
INSERT INTO `czit_auth_admin` VALUES ('13', 'admini', 'd23735424a541d8f7b1e25a58b6b408b', '2166', '113.87.163.243', '1503366101', '1', '4', '4ee2e395e9921f515d00599a5f79ae3f', null);

-- ----------------------------
-- Table structure for `czit_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `czit_auth_group`;
CREATE TABLE `czit_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of czit_auth_group
-- ----------------------------
INSERT INTO `czit_auth_group` VALUES ('1', '超级管理员', '1', '9,41,42,43,44,45,10,39,40');
INSERT INTO `czit_auth_group` VALUES ('4', '系统测试员', '1', '4,2,6,46,47,48,49,50,51,9,41,42,43,44,45,10,39');

-- ----------------------------
-- Table structure for `czit_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `czit_auth_group_access`;
CREATE TABLE `czit_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of czit_auth_group_access
-- ----------------------------
INSERT INTO `czit_auth_group_access` VALUES ('1', '1');
INSERT INTO `czit_auth_group_access` VALUES ('13', '4');

-- ----------------------------
-- Table structure for `czit_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `czit_auth_rule`;
CREATE TABLE `czit_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `pid` tinyint(5) DEFAULT '0',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of czit_auth_rule
-- ----------------------------
INSERT INTO `czit_auth_rule` VALUES ('6', '4444', 'admini/index/console', '1', '2', '', '2', '4', '4');
INSERT INTO `czit_auth_rule` VALUES ('2', '学生列表', 'admini/student/index', '1', '1', '', '4', '1', '111');
INSERT INTO `czit_auth_rule` VALUES ('4', '学生管理', '#', '1', '1', '', '0', '1', '');
INSERT INTO `czit_auth_rule` VALUES ('9', '老师管理', '#', '1', '1', '', '0', '2', '');
INSERT INTO `czit_auth_rule` VALUES ('10', '家长管理', '#', '1', '1', '', '0', '3', '&#xe604;');
INSERT INTO `czit_auth_rule` VALUES ('11', '生活交费', '#', '1', '1', '', '0', '4', '&#xe60c;');
INSERT INTO `czit_auth_rule` VALUES ('12', '在线教育', '#', '1', '1', '', '0', '5', '&#xe60a;');
INSERT INTO `czit_auth_rule` VALUES ('13', '就业情况', '#', '1', '1', '', '0', '6', '&#xe606;');
INSERT INTO `czit_auth_rule` VALUES ('14', '官网管理', '#', '1', '1', '', '0', '7', '&#xe607;');
INSERT INTO `czit_auth_rule` VALUES ('15', '系统设置', '#', '1', '1', '', '0', '8', '');
INSERT INTO `czit_auth_rule` VALUES ('16', '管理员管理', 'admini/admini/admini', '1', '1', '', '15', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('17', '菜单权限', 'admini/admini/rule', '1', '1', '', '15', '2', '2');
INSERT INTO `czit_auth_rule` VALUES ('18', '权限组', 'admini/admini/group', '1', '1', '', '15', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('19', '联系方式', 'admini/web/about', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('20', '热门课程', 'admini/web/hotcourse', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('21', '学院动态', 'admini/web/news', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('22', '学员就业', 'admini/web/job', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('23', '招生问答', 'admini/web/answers', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('24', '通知公告', 'admini/web/notice', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('25', '在线留言', 'admini/web/message', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('26', '广告管理', 'admini/web/ads', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('27', '毕业学生', '1', '1', '1', '', '13', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('28', '实习生', '1', '1', '1', '', '13', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('29', '企业招聘', '1', '1', '1', '', '13', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('30', '面试问题', '1', '1', '1', '', '13', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('31', '毕业统计', '1', '1', '1', '', '13', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('32', '广告列表', '1', '1', '1', '', '12', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('33', '课程分类', '1', '1', '1', '', '12', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('34', '课程列表', '1', '1', '1', '', '12', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('35', '单课列表', '1', '1', '1', '', '12', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('36', '水费列表', '1', '1', '1', '', '11', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('37', '电费列表', '1', '1', '1', '', '11', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('38', '餐费列表', '1', '1', '1', '', '11', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('39', '家长评价', '1', '1', '1', '', '10', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('40', '建议信', '1', '1', '1', '', '10', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('41', '老师列表', 'admini/teacher/index', '1', '1', '', '9', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('42', '班级管理', '1', '1', '1', '', '9', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('43', '学课管理', '1', '1', '1', '', '9', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('44', '班级进度', '1', '1', '1', '', '9', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('45', '教育问题', '1', '1', '1', '', '9', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('46', '学分列表', 'admini/student/xuefen', '1', '1', '', '4', '4', '1');
INSERT INTO `czit_auth_rule` VALUES ('47', '成绩列表', '1', '1', '1', '', '4', '5', '1');
INSERT INTO `czit_auth_rule` VALUES ('48', '奖惩记录', 'admini/student/jiangcheng', '1', '1', '', '4', '3', '1');
INSERT INTO `czit_auth_rule` VALUES ('49', '回家登记', 'admini/student/dengji', '1', '1', '', '4', '2', '1');
INSERT INTO `czit_auth_rule` VALUES ('50', '学费管理', '1', '1', '1', '', '4', '6', '1');
INSERT INTO `czit_auth_rule` VALUES ('51', '学生评价', '1', '1', '1', '', '4', '7', '1');
INSERT INTO `czit_auth_rule` VALUES ('54', '在线报名', 'admini/web/baoming', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('55', '学校简介', 'admini/web/alist', '1', '1', '', '14', '1', '1');
INSERT INTO `czit_auth_rule` VALUES ('56', '新增', 'admini/web/news_add', '1', '1', '', '21', '1', '1');

-- ----------------------------
-- Table structure for `czit_gw_about`
-- ----------------------------
DROP TABLE IF EXISTS `czit_gw_about`;
CREATE TABLE `czit_gw_about` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '1' COMMENT '1学校简介2热门课程',
  `keys` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `click` int(11) NOT NULL DEFAULT '1' COMMENT '点击率',
  `add_time` datetime NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='学校简介、热门课程';

-- ----------------------------
-- Records of czit_gw_about
-- ----------------------------
INSERT INTO `czit_gw_about` VALUES ('0000000006', '2', '991111', 'PHP工程师', 'PHP工程师PHP工程师PHP工程师999', '1', '2019-01-02 08:54:11');
INSERT INTO `czit_gw_about` VALUES ('0000000007', '2', 'ppp', 'JAVA工程师', 'JAVA工程师JAVA工程师JAVA工程师', '1', '2019-01-02 08:54:26');
INSERT INTO `czit_gw_about` VALUES ('0000000008', '2', '', '前端UI工程师', '前端UI工程师', '1', '2019-01-02 08:54:39');
INSERT INTO `czit_gw_about` VALUES ('0000000009', '1', '联系方式', '联系方式', '联系方式联系方式', '1', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `czit_gw_ads`
-- ----------------------------
DROP TABLE IF EXISTS `czit_gw_ads`;
CREATE TABLE `czit_gw_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of czit_gw_ads
-- ----------------------------
INSERT INTO `czit_gw_ads` VALUES ('1', '首页', '#', '/uploads/upload/20190110/4a185452b0d73278e092885e6171eb23.png', 'index', '1');
INSERT INTO `czit_gw_ads` VALUES ('2', '学校简介', '#', '/uploads/upload/20190116/8c8bbdaae0e9a88cbe67b71ae905e5dc.png', 'about', '3');
INSERT INTO `czit_gw_ads` VALUES ('3', '热门课程', '#', '/uploads/upload/20190116/bb3c093a2d8a9afc32c2781ebf82f305.png', 'hotcourse', '3');
INSERT INTO `czit_gw_ads` VALUES ('4', '学院动态', '#', '/uploads/upload/20190116/99c4d29c0b3a7711e6b97635019e1fbe.png', 'newslist', '4');
INSERT INTO `czit_gw_ads` VALUES ('5', '学员就业', '#', '/uploads/upload/20190116/a243c8890a5912b48f75cf7360415fbb.png', 'joblist', '5');
INSERT INTO `czit_gw_ads` VALUES ('6', '就业招生', '#', '/uploads/upload/20190116/efe2d597017c3dd2ece42a369413fccb.png', 'answerslist', '6');
INSERT INTO `czit_gw_ads` VALUES ('7', '通知公告', '#', '/uploads/upload/20190116/8481c2cfa0d079016be6c13170ec2ae8.jpg', 'notice', '7');
INSERT INTO `czit_gw_ads` VALUES ('9', '首页', '#', '/uploads/upload/20190110/6bdc862f2ec9345e225cdc8eba246f30.png', 'index', '2');

-- ----------------------------
-- Table structure for `czit_gw_alist`
-- ----------------------------
DROP TABLE IF EXISTS `czit_gw_alist`;
CREATE TABLE `czit_gw_alist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '1' COMMENT '简介1学校环境2师资力量3荣誉4校企',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `images` varchar(255) NOT NULL COMMENT '图片',
  `content` text NOT NULL COMMENT '内容',
  `add_time` datetime NOT NULL COMMENT '增加时间',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='学校简介';

-- ----------------------------
-- Records of czit_gw_alist
-- ----------------------------
INSERT INTO `czit_gw_alist` VALUES ('65', '1', '55555555', '', '5555555555555', '2019-01-16 17:27:16', '100');
INSERT INTO `czit_gw_alist` VALUES ('40', '2', '435453435453543777', '/uploads/upload/20190107/171c484294f491eb95f4ae62bc9d5da5.png', '44453453534', '2019-01-05 10:25:16', '100');
INSERT INTO `czit_gw_alist` VALUES ('41', '2', '学院设立迎新服务点，由班车直接从迎新服务点接至学院。', '/uploads/upload/20190107/171c484294f491eb95f4ae62bc9d5da5.png', '<p><strong><span>一、新生报到时间</span></strong></p><p><br></p><p>2017年8月19日(如有变更以录取通知书上通知的报名时间为准)，统一到校报到。</p><p><br></p><p><strong><span>二、入学报到当天须准备的资料及物品</span></strong></p><p><br></p><p>* 携带录取通知书；</p><p><br></p><p>* 本人以及法定监护人身份证原件以及复印件2张；</p><p><br></p><p>* 户口本原件以及户主、本人页复印件2张；</p><p><br></p><p>* 需要提供学历证明(最高学历)原件和复印件2张；</p><p><br></p><p>* 一寸白底彩色免冠照片8张；</p><p><br></p><p>* 报到当天统一分配住宿，但学生须提前准备好被褥及其它生活用品。</p><p><br></p><p><strong><span>三、如果未在规定时间内报到怎么办？</span></strong></p><p><br></p><p>因故不能按规定时间报到注册者，应向我院招生办请假；假期最多不得超过两周；未经请假或请假逾期报到者以旷课论处，超过两周不报到者，取消入学资格。</p><p><br></p><p><strong><span>四、报到期间是否有人接站？乘车路线？</span></strong></p><p><br></p><p>报到期间，我校会在湘南学院设立迎新服务点，由班车直接从迎新服务点接至学院。</p><p><br></p><p><strong>乘车路线：</strong></p><p><br></p><p>郴州汽车总站：52路到火车站下车，转13,21,22,27,36路到湘南学院站下车;</p><p><br></p><p>火车站乘：13路，21路，22路，27路，36路到湘南学院站下;</p>', '2019-01-05 10:31:18', '100');
INSERT INTO `czit_gw_alist` VALUES ('36', '4', '43435345435', '', '45545453435', '2019-01-05 10:24:54', '100');
INSERT INTO `czit_gw_alist` VALUES ('37', '4', '43435543453', '', '545454534', '2019-01-05 10:24:59', '100');
INSERT INTO `czit_gw_alist` VALUES ('38', '4', '43435453345', '', '43543543545', '2019-01-05 10:25:04', '100');
INSERT INTO `czit_gw_alist` VALUES ('39', '4', '345453435435', '', '54546546546', '2019-01-05 10:25:11', '100');
INSERT INTO `czit_gw_alist` VALUES ('42', '6', '3243242', '', '', '2019-01-07 10:57:55', '100');
INSERT INTO `czit_gw_alist` VALUES ('43', '6', '2342342300', '', 'sdssds', '2019-01-07 10:58:43', '100');
INSERT INTO `czit_gw_alist` VALUES ('44', '4', '234', '', '', '2019-01-07 10:59:08', '100');
INSERT INTO `czit_gw_alist` VALUES ('45', '4', 'sdf', '', '', '2019-01-07 10:59:25', '100');
INSERT INTO `czit_gw_alist` VALUES ('46', '5', '4322323', '', '', '2019-01-07 11:01:23', '100');
INSERT INTO `czit_gw_alist` VALUES ('47', '2', '543345', '/uploads/upload/20190107/171c484294f491eb95f4ae62bc9d5da5.png', '', '2019-01-07 11:06:09', '100');
INSERT INTO `czit_gw_alist` VALUES ('48', '4', '3445', '', '', '2019-01-07 11:06:19', '100');
INSERT INTO `czit_gw_alist` VALUES ('49', '4', '3445', '', '', '2019-01-07 11:06:23', '100');
INSERT INTO `czit_gw_alist` VALUES ('50', '4', '3445', '', '', '2019-01-07 11:06:24', '5');
INSERT INTO `czit_gw_alist` VALUES ('51', '4', '3445', '', '', '2019-01-07 11:06:24', '100');
INSERT INTO `czit_gw_alist` VALUES ('52', '4', '3445', '', '', '2019-01-07 11:06:25', '3');
INSERT INTO `czit_gw_alist` VALUES ('53', '4', '7766767', '', '', '2019-01-07 11:08:44', '4');
INSERT INTO `czit_gw_alist` VALUES ('54', '5', 'ertertree', '', '', '2019-01-07 11:09:11', '1');
INSERT INTO `czit_gw_alist` VALUES ('55', '2', 'ertert', '/uploads/upload/20190107/171c484294f491eb95f4ae62bc9d5da5.png', '', '2019-01-07 11:09:21', '2');
INSERT INTO `czit_gw_alist` VALUES ('58', '9', '3443', '', 'sddsds', '2019-01-07 11:11:46', '100');
INSERT INTO `czit_gw_alist` VALUES ('60', '8', '534511111111111', '', '', '2019-01-09 08:45:13', '100');
INSERT INTO `czit_gw_alist` VALUES ('61', '8', '223324', '', '', '2019-01-09 11:22:22', '100');
INSERT INTO `czit_gw_alist` VALUES ('62', '8', '34342432eeeeeeeeeeeeeeeeeeee', '', '', '2019-01-09 11:23:02', '100');
INSERT INTO `czit_gw_alist` VALUES ('63', '4', 'ewewewerewr', '/uploads/upload/20190112/7e7f2133e0dbd076be43d07c33a96387.png', '<p><span>代，由于网速和终端能力的限制，大部分网站只能呈现简单的图文信息，并不能满足用户在界面上的需求，对界面技术的要求也不高。随着硬件的完善、高性能浏览器的出现和宽带的普及，技术可以在用户体验方面实现更多种可能，前端技术领域迸发出旺盛的生命力。</span></p>', '2019-01-12 09:54:43', '100');
INSERT INTO `czit_gw_alist` VALUES ('64', '6', '111', '/uploads/upload/20190115/8c1226a6ce5d993ac1c06f74cfb51e52.png', '<img src=\"/uploads/edit/20190115/fe21c7dc302cf4bde4f2fb8101eaf000.png\" alt=\"QQ截图20190112092039.png\">', '2019-01-15 17:11:31', '100');

-- ----------------------------
-- Table structure for `czit_gw_message`
-- ----------------------------
DROP TABLE IF EXISTS `czit_gw_message`;
CREATE TABLE `czit_gw_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '1' COMMENT '1留言2公告3报名',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `add_time` datetime DEFAULT NULL COMMENT '增加时间',
  `tel` varchar(12) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '2' COMMENT '1审核 2未审核',
  `km` tinyint(1) DEFAULT '1' COMMENT '学习科目1php2java3ui',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='通知公告';

-- ----------------------------
-- Records of czit_gw_message
-- ----------------------------
INSERT INTO `czit_gw_message` VALUES ('3', '2', 'ret', '', '2019-01-07 11:34:11', null, '2', '1');
INSERT INTO `czit_gw_message` VALUES ('6', '2', '345345', '', '2019-01-09 08:44:51', null, '2', '1');
INSERT INTO `czit_gw_message` VALUES ('7', '2', '12', '22222222222222', '2019-01-12 08:33:04', null, '2', '1');
INSERT INTO `czit_gw_message` VALUES ('10', '1', '24234', '34234', '2019-01-15 15:52:54', '2342', '1', '1');
INSERT INTO `czit_gw_message` VALUES ('11', '1', 'phpphp', '123123', '2019-01-16 11:27:47', '123123', '2', '1');
INSERT INTO `czit_gw_message` VALUES ('12', '1', 'java', '2342', '2019-01-16 11:27:57', '234234', '2', '2');
INSERT INTO `czit_gw_message` VALUES ('13', '3', '2342', '234', '2019-01-16 11:29:08', '234234', '2', '2');
INSERT INTO `czit_gw_message` VALUES ('14', '3', '53345', '345', '2019-01-16 11:29:15', '345345', '2', '3');

-- ----------------------------
-- Table structure for `czit_gw_news`
-- ----------------------------
DROP TABLE IF EXISTS `czit_gw_news`;
CREATE TABLE `czit_gw_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL COMMENT '父类',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `info` varchar(255) DEFAULT NULL COMMENT '简介',
  `images` varchar(255) NOT NULL COMMENT '图片',
  `content` text NOT NULL COMMENT '内容',
  `click` int(11) NOT NULL DEFAULT '1' COMMENT '点击率',
  `add_time` datetime NOT NULL COMMENT '增加时间',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  `keys` varchar(255) DEFAULT NULL COMMENT '关键字',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COMMENT='学院动态、学员就业、招生问答';

-- ----------------------------
-- Records of czit_gw_news
-- ----------------------------
INSERT INTO `czit_gw_news` VALUES ('7', '2', '123', null, '123', '123', '1', '0000-00-00 00:00:00', null, null);
INSERT INTO `czit_gw_news` VALUES ('6', '1', '123', null, '123', '123', '1', '0000-00-00 00:00:00', null, null);
INSERT INTO `czit_gw_news` VALUES ('40', '4', '435453435453543777', null, '/uploads/upload/20190107/171c484294f491eb95f4ae62bc9d5da5.png', '44453453534', '26', '2019-01-05 10:25:16', '100', '');
INSERT INTO `czit_gw_news` VALUES ('41', '4', '学院设立迎新服务点，由班车直接从迎新服务点接至学院。', null, '', '<p><strong><span>一、新生报到时间</span></strong></p><p><br></p><p>2017年8月19日(如有变更以录取通知书上通知的报名时间为准)，统一到校报到。</p><p><br></p><p><strong><span>二、入学报到当天须准备的资料及物品</span></strong></p><p><br></p><p>* 携带录取通知书；</p><p><br></p><p>* 本人以及法定监护人身份证原件以及复印件2张；</p><p><br></p><p>* 户口本原件以及户主、本人页复印件2张；</p><p><br></p><p>* 需要提供学历证明(最高学历)原件和复印件2张；</p><p><br></p><p>* 一寸白底彩色免冠照片8张；</p><p><br></p><p>* 报到当天统一分配住宿，但学生须提前准备好被褥及其它生活用品。</p><p><br></p><p><strong><span>三、如果未在规定时间内报到怎么办？</span></strong></p><p><br></p><p>因故不能按规定时间报到注册者，应向我院招生办请假；假期最多不得超过两周；未经请假或请假逾期报到者以旷课论处，超过两周不报到者，取消入学资格。</p><p><br></p><p><strong><span>四、报到期间是否有人接站？乘车路线？</span></strong></p><p><br></p><p>报到期间，我校会在湘南学院设立迎新服务点，由班车直接从迎新服务点接至学院。</p><p><br></p><p><strong>乘车路线：</strong></p><p><br></p><p>郴州汽车总站：52路到火车站下车，转13,21,22,27,36路到湘南学院站下车;</p><p><br></p><p>火车站乘：13路，21路，22路，27路，36路到湘南学院站下;</p>', '17', '2019-01-05 10:31:18', '100', '');
INSERT INTO `czit_gw_news` VALUES ('36', '4', '43435345435', null, '', '45545453435', '1', '2019-01-05 10:24:54', '100', '');
INSERT INTO `czit_gw_news` VALUES ('37', '4', '43435543453', null, '', '545454534', '8', '2019-01-05 10:24:59', '100', '');
INSERT INTO `czit_gw_news` VALUES ('38', '4', '43435453345', null, '', '43543543545', '1', '2019-01-05 10:25:04', '100', '');
INSERT INTO `czit_gw_news` VALUES ('39', '4', '345453435435', null, '', '54546546546', '1', '2019-01-05 10:25:11', '100', '');
INSERT INTO `czit_gw_news` VALUES ('42', '6', '3243242', null, '', '', '19', '2019-01-07 10:57:55', '100', '');
INSERT INTO `czit_gw_news` VALUES ('43', '6', '2342342300', null, '', 'sdssds', '7', '2019-01-07 10:58:43', '100', '');
INSERT INTO `czit_gw_news` VALUES ('44', '4', '234', null, '', '', '1', '2019-01-07 10:59:08', '100', '');
INSERT INTO `czit_gw_news` VALUES ('45', '4', 'sdf', null, '', '', '1', '2019-01-07 10:59:25', '100', '');
INSERT INTO `czit_gw_news` VALUES ('46', '5', '4322323', null, '', '', '4', '2019-01-07 11:01:23', '100', '');
INSERT INTO `czit_gw_news` VALUES ('47', '4', '543345', null, '', '', '1', '2019-01-07 11:06:09', '100', '');
INSERT INTO `czit_gw_news` VALUES ('48', '4', '3445', null, '', '', '1', '2019-01-07 11:06:19', '100', '');
INSERT INTO `czit_gw_news` VALUES ('49', '4', '3445', null, '', '', '1', '2019-01-07 11:06:23', '100', '');
INSERT INTO `czit_gw_news` VALUES ('50', '4', '3445', null, '', '', '1', '2019-01-07 11:06:24', '5', '');
INSERT INTO `czit_gw_news` VALUES ('51', '4', '3445', null, '', '', '1', '2019-01-07 11:06:24', '100', '');
INSERT INTO `czit_gw_news` VALUES ('52', '4', '3445', null, '', '', '1', '2019-01-07 11:06:25', '3', '');
INSERT INTO `czit_gw_news` VALUES ('65', '4', '1111', '', '', '<p><img src=\"/ueditor/20190221/eb9c99c96b76a5f5b7a2419b3ab8cd99.png\" title=\"eb9c99c96b76a5f5b7a2419b3ab8cd99.png\" alt=\"eb9c99c96b76a5f5b7a2419b3ab8cd99.png\"/></p><p><br/></p><p>11111</p>', '1', '2019-02-21 10:00:02', '100', '1111');
INSERT INTO `czit_gw_news` VALUES ('54', '5', 'ertertree', null, '', '', '1', '2019-01-07 11:09:11', '1', '');
INSERT INTO `czit_gw_news` VALUES ('55', '4', 'ertert', null, '', '', '1', '2019-01-07 11:09:21', '2', '');
INSERT INTO `czit_gw_news` VALUES ('58', '9', '3443', null, '', 'sddsds', '3', '2019-01-07 11:11:46', '100', '');
INSERT INTO `czit_gw_news` VALUES ('60', '8', '534511111111111', null, '', '', '8', '2019-01-09 08:45:13', '100', '');
INSERT INTO `czit_gw_news` VALUES ('61', '8', '223324', null, '', '', '1', '2019-01-09 11:22:22', '100', '');
INSERT INTO `czit_gw_news` VALUES ('62', '8', '34342432eeeeeeeeeeeeeeeeeeee', null, '', '', '1', '2019-01-09 11:23:02', '100', '');
INSERT INTO `czit_gw_news` VALUES ('63', '4', 'ewewewerewr', null, '/uploads/upload/20190112/7e7f2133e0dbd076be43d07c33a96387.png', '<p><span>代，由于网速和终端能力的限制，大部分网站只能呈现简单的图文信息，并不能满足用户在界面上的需求，对界面技术的要求也不高。随着硬件的完善、高性能浏览器的出现和宽带的普及，技术可以在用户体验方面实现更多种可能，前端技术领域迸发出旺盛的生命力。</span></p>', '1', '2019-01-12 09:54:43', '100', '3345345');
INSERT INTO `czit_gw_news` VALUES ('64', '6', '111', null, '/uploads/upload/20190115/8c1226a6ce5d993ac1c06f74cfb51e52.png', '<img src=\"/uploads/edit/20190115/fe21c7dc302cf4bde4f2fb8101eaf000.png\" alt=\"QQ截图20190112092039.png\">', '1', '2019-01-15 17:11:31', '100', '1111');
INSERT INTO `czit_gw_news` VALUES ('66', '5', '55555', '5', '', '<p>55555555555555</p>', '1', '2019-02-21 10:21:41', '100', '');

-- ----------------------------
-- Table structure for `czit_gw_newscate`
-- ----------------------------
DROP TABLE IF EXISTS `czit_gw_newscate`;
CREATE TABLE `czit_gw_newscate` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL COMMENT '父ID',
  `cate_name` varchar(255) NOT NULL COMMENT '类名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of czit_gw_newscate
-- ----------------------------
INSERT INTO `czit_gw_newscate` VALUES ('0000000001', '0', '学院动态');
INSERT INTO `czit_gw_newscate` VALUES ('0000000002', '0', '学员就业');
INSERT INTO `czit_gw_newscate` VALUES ('0000000003', '0', '招生问答');
INSERT INTO `czit_gw_newscate` VALUES ('0000000004', '1', '校园动态');
INSERT INTO `czit_gw_newscate` VALUES ('0000000005', '1', '行业前景');
INSERT INTO `czit_gw_newscate` VALUES ('0000000006', '2', '学员作品');
INSERT INTO `czit_gw_newscate` VALUES ('0000000007', '2', '学员活动');
INSERT INTO `czit_gw_newscate` VALUES ('0000000008', '3', '校区问答');
INSERT INTO `czit_gw_newscate` VALUES ('0000000009', '3', '就业问答');
INSERT INTO `czit_gw_newscate` VALUES ('0000000010', '3', '课程问答');

-- ----------------------------
-- Table structure for `czit_oa_classname`
-- ----------------------------
DROP TABLE IF EXISTS `czit_oa_classname`;
CREATE TABLE `czit_oa_classname` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `class_name` varchar(50) DEFAULT NULL COMMENT '班级年级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='班级年级';

-- ----------------------------
-- Records of czit_oa_classname
-- ----------------------------
INSERT INTO `czit_oa_classname` VALUES ('1', '0', '17年级');
INSERT INTO `czit_oa_classname` VALUES ('2', '1', '1709班');
INSERT INTO `czit_oa_classname` VALUES ('3', '0', '16年级');
INSERT INTO `czit_oa_classname` VALUES ('4', '3', '1601班');

-- ----------------------------
-- Table structure for `czit_oa_student`
-- ----------------------------
DROP TABLE IF EXISTS `czit_oa_student`;
CREATE TABLE `czit_oa_student` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名（电话）',
  `password` varchar(32) DEFAULT 'e6de76125d78b81a9b81fc8fd6c57c3b' COMMENT '密码',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `status` tinyint(2) DEFAULT '2' COMMENT '1报名2学习中3毕业4实习中5上班',
  `images` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` int(11) DEFAULT '1' COMMENT '1男2女',
  `tel` int(11) DEFAULT NULL COMMENT '父母手机号',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `area` varchar(255) DEFAULT NULL COMMENT '区县',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `age` date DEFAULT NULL COMMENT '年龄',
  `classes` int(11) DEFAULT NULL COMMENT '班级',
  `grade` int(11) DEFAULT NULL COMMENT '年级',
  `add_time` datetime DEFAULT NULL COMMENT '新增时间',
  `in_time` date DEFAULT NULL COMMENT '入校时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='学生信息';

-- ----------------------------
-- Records of czit_oa_student
-- ----------------------------
INSERT INTO `czit_oa_student` VALUES ('2', '788', 'd23735424a541d8f7b1e25a58b6b408b', '77878', '1', '/uploads/upload/20190110/efe1e02c1602d0ed2a598357f650cd46.jpg', '0', '7', '140000_11_3', '140300_5_2', '140303', '7788778887', '0000-00-00', null, '4', '2019-01-11 00:00:00', '2019-01-11');
INSERT INTO `czit_oa_student` VALUES ('3', '89', 'd23735424a541d8f7b1e25a58b6b408b', '898989', '1', '', '0', '8', '130000_11_2', '130300_7_2', '130304', '89899', '0000-00-00', null, '4', '2019-01-10 00:00:00', '2019-01-10');
INSERT INTO `czit_oa_student` VALUES ('6', '6', 'd23735424a541d8f7b1e25a58b6b408b', '66767', '1', '', '0', '67', '130000_11_2', '130200_14_1', '130203', '676767', '0000-00-00', null, '2', '2019-01-11 00:00:00', '2019-01-11');
INSERT INTO `czit_oa_student` VALUES ('7', '78', 'd23735424a541d8f7b1e25a58b6b408b', '78778', '1', '', '1', '78', '120000_2_1', '120200_3_1', '120223', '78787', '0000-00-00', null, '2', '2019-01-03 00:00:00', '2019-01-03');
INSERT INTO `czit_oa_student` VALUES ('8', '767', 'd23735424a541d8f7b1e25a58b6b408b', '676776', '1', '', '1', '6', '120000_2_1', '120200_3_1', '120223', '6766776', '0000-00-00', null, '2', '2019-01-09 00:00:00', '2019-01-09');
INSERT INTO `czit_oa_student` VALUES ('10', '88888888888888', 'd23735424a541d8f7b1e25a58b6b408b', '888888', '1', '', '1', '2147483647', '130000_11_2', '130300_7_2', '130303', '8888888888', '0000-00-00', null, '2', '2019-01-10 00:00:00', '2019-01-10');
INSERT INTO `czit_oa_student` VALUES ('11', '888888', 'd23735424a541d8f7b1e25a58b6b408b', '8888', '1', '', '1', '888888888', '120000_2_1', '120200_3_1', '120221', '888888888888888', '0000-00-00', null, '2', '2019-01-10 00:00:00', '2019-01-10');
INSERT INTO `czit_oa_student` VALUES ('15', '7878', 'd23735424a541d8f7b1e25a58b6b408b', '778', '1', '', '1', '78', '120000_2_1', '120200_3_1', '120223', '7878877887', '0000-00-00', null, '2', '2019-01-10 00:00:00', '2019-01-10');
INSERT INTO `czit_oa_student` VALUES ('16', '345', 'd23735424a541d8f7b1e25a58b6b408b', '3345', '1', '', '1', '345', '120000_2_1', '120200_3_1', '120221', '345', '0000-00-00', null, '2', '2019-01-10 00:00:00', '2019-01-10');
INSERT INTO `czit_oa_student` VALUES ('17', '234', 'd23735424a541d8f7b1e25a58b6b408b', '3324', '1', '', '1', '234', '120000_2_1', '120200_3_1', '120223', '234444444', '0000-00-00', null, '2', '2018-12-31 00:00:00', '2018-12-31');
INSERT INTO `czit_oa_student` VALUES ('18', '76', 'd23735424a541d8f7b1e25a58b6b408b', '6767', '1', '', '1', '6767', '天津市_2_1', '天津市辖县_3_1', '静海县', '67677676', '0000-00-00', null, '2', '2019-01-09 00:00:00', '2019-01-09');
INSERT INTO `czit_oa_student` VALUES ('32', '444444444', 'e6de76125d78b81a9b81fc8fd6c57c3b', '324444', '1', '', '2', '444444444', '天津市_2_1', '天津市辖县_3_1', '静海县', '423432323243', '2019-01-17', null, '4', '2019-01-11 09:11:48', '2019-01-11');
INSERT INTO `czit_oa_student` VALUES ('29', '234', 'd23735424a541d8f7b1e25a58b6b408b', '234243', '2', '', '2', '234', '天津市_2_1', '天津市辖县_3_1', '蓟县', '234234', '0000-00-00', null, '4', '2019-01-09 00:00:00', '2019-01-09');
INSERT INTO `czit_oa_student` VALUES ('30', '111111111', 'd23735424a541d8f7b1e25a58b6b408b', '111111111111', '2', '/uploads/upload/20190110/2d7e1cd234c292229c82bf517a27e34e.png', '2', '1111111111', '天津市_2_1', '天津市_13_0', '和平区', '11111111111111111', '0000-00-00', null, '4', '2019-01-08 00:00:00', '2019-01-08');
INSERT INTO `czit_oa_student` VALUES ('31', '67', 'e6de76125d78b81a9b81fc8fd6c57c3b', '676767', '2', '/uploads/upload/20190111/1fac4623023c1489e10b0eb56b1cf2ed.jpg', '2', '67', '河北省_11_2', '秦皇岛市_7_2', '山海关区', '67676767677667', '2019-01-11', null, '4', '2019-01-11 08:36:57', '2019-01-11');
INSERT INTO `czit_oa_student` VALUES ('33', '324342', 'e6de76125d78b81a9b81fc8fd6c57c3b', '342342343', '2', '', '2', '23342342', '山西省_11_3', '阳泉市_5_2', '矿区', '334234342', '2019-01-12', null, '4', '2019-01-11 15:09:00', '2019-01-10');

-- ----------------------------
-- Table structure for `czit_oa_teacher`
-- ----------------------------
DROP TABLE IF EXISTS `czit_oa_teacher`;
CREATE TABLE `czit_oa_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `teacher_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `images` varchar(255) DEFAULT NULL COMMENT '头像',
  `grade` int(11) DEFAULT NULL COMMENT '年级',
  `tel` int(12) DEFAULT NULL COMMENT '家庭电话',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别',
  `age` varchar(20) DEFAULT NULL COMMENT '年龄',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `area` varchar(255) DEFAULT NULL COMMENT '区县',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `add_time` datetime DEFAULT NULL COMMENT '增加时间',
  `in_time` date DEFAULT NULL COMMENT '入校时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '1在职2离职',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='老师信息';

-- ----------------------------
-- Records of czit_oa_teacher
-- ----------------------------
INSERT INTO `czit_oa_teacher` VALUES ('1', '345', null, '4354', '', '4', '345', '1', '2019-01-10', '河北省_11_2', '邯郸市_19_3', '丛台区', '3455555555', '2019-01-11 17:00:57', '2019-01-08', '1');
